import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

/**
 * @author Blidar George
 * Class Name: CalculatorGUI 
 * Descriere: Este cea in care se realizeaza toate elementele ce apartin de implementarea interfetei
 * grafice a acestei teme. Implementarea interfetei ActionListener este realizata de catre clasa
 * in sine.
 */
public class CalculatorGUI extends JPanel implements ActionListener {
	
	
	private static final long serialVersionUID = 1L;

	/**
	 * Method name: triggerWindow()
	 * Descriere: Este o metoda statica care are rolul de a declansa / genera frame-ul in care
	 * se vor efectua operatiile pe polinoame, ori de cate ori programul este rulat din clasa Main.
	 */
	public static void triggerWindow()
	{
		JFrame window = new JFrame("Calculator de Polinoame");
		CalculatorGUI content = new CalculatorGUI();
		window.setContentPane(content);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setSize(1000,300);
		window.setLocation(100,300);
		window.setVisible(true);
		window.setEnabled(true);
	}
	
	JTextField firstPolField;
	JTextField secondPolField;
	JTextField rezultatField;
	JScrollPane scroll;
	JLabel firstPolLabel;
	JLabel secondPolLabel;
	JLabel rezultatLabel;
	JButton adunare;
	JButton scadere;
	JButton inmultire;
	JButton impartire;
	JButton derivare;
	JButton integrare;
	
	/**
	 * Constructorul clasei: Seteaza si aseaza toate componentele GUI in JFrame-ul aplicatiei.
	 */
	public CalculatorGUI()
	{
		this.setBackground(new Color(125,20,13));
		this.setLayout(new GridLayout(1,2));
		
		JPanel leftPanel = new JPanel();
		leftPanel.setBackground(new Color(125,150,13));
		leftPanel.setLayout(new GridLayout(3,2));
		this.add(leftPanel);
		
		firstPolLabel = new JLabel("Primul polinom:");
		firstPolLabel.setFont(new Font("SERIF",Font.PLAIN,18));
		leftPanel.add(firstPolLabel);
		
		firstPolField = new JTextField();
		firstPolField.setFont(new Font("Comic Sans MS",Font.BOLD,15));
		leftPanel.add(firstPolField);
		
		secondPolLabel = new JLabel("Al doilea polinom:");
		secondPolLabel.setFont(new Font("SERIF",Font.PLAIN,18));
		leftPanel.add(secondPolLabel);
		
		secondPolField = new JTextField();
		secondPolField.setFont(new Font("Comic Sans MS",Font.BOLD,15));
		leftPanel.add(secondPolField);
		
		rezultatLabel = new JLabel("Rezultatul:");
		rezultatLabel.setFont(new Font("SERIF",Font.BOLD,18));
		leftPanel.add(rezultatLabel);
		
		rezultatField = new JTextField();
		rezultatField.setEditable(false);
		rezultatField.setFont(new Font("Comic Sans MS",Font.BOLD,15));
		leftPanel.add(rezultatField);
		
		scroll = new JScrollPane(rezultatField);
		leftPanel.add(scroll);
		
		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new GridLayout(2,3));
		rightPanel.setBackground(new Color(125,150,13));
		this.add(rightPanel);
		
		adunare = new JButton("Adunare");
		adunare.addActionListener(this);
		rightPanel.add(adunare);
		
		scadere = new JButton("Scadere");
		scadere.addActionListener(this);
		rightPanel.add(scadere);
		
		inmultire = new JButton("Inmultire");
		inmultire.addActionListener(this);
		rightPanel.add(inmultire);
		
		impartire = new JButton("Impartire");
		impartire.addActionListener(this);
		rightPanel.add(impartire);
		
		derivare = new JButton("Derivare");
		derivare.addActionListener(this);
		rightPanel.add(derivare);
		
		integrare = new JButton("Integrare");
		integrare.addActionListener(this);
		rightPanel.add(integrare);
	}
	
	/**
	 * Method name: actionPerformed(ActionEvent evt)
	 * Descriere: Este metoda ce trebuie scrisa ori de cate ori o clasa implementeaza interfata
	 * ActionListener. Aici se verifica care este butonul pe care-l apasa utilizatorul aplicatiei
	 * si se ia o decizie in functie de acest lucru (se face adunare, scadere...)
	 */
	public void actionPerformed(ActionEvent evt)
	{
		String string = evt.getActionCommand();
		switch(string)
		{
		case "Adunare" :
			doAdunare();
			break;
		case "Scadere" :
			doScadere();
			break;
		case "Inmultire" :
			doInmultire();
			break;
		case "Impartire" :
			doImpartire();
			break;
		case "Derivare" :
			try
			{
				doDerivare();
			}
			catch(DerivareException ex)
			{
				ex.printStackTrace();
			}
			break;
		case "Integrare" :
			try
			{
				doIntegrare();
			}
			catch(IntegrareException ex)
			{
				ex.printStackTrace();
			}
			catch(IllegalArgumentException e)
			{
				e.printStackTrace();
			}
			break;
		}
		
	}
	
	/**
	 * Methods name: doAdunare(), doScadere(), doInmultire(), doImpartire(), doDerivare(), doIntegrare()
	 * Descriere: Toate aceste metode au un corp asemanator, exceptie facand apelul metodei statice
	 * din clasa Operatie, in functie de operatia dorita asupra polinoamelor. Se preiau textele
	 * din cele doua campuri aferente polinoamelor (JTextFields), sub forma de String-uri, iar cu ajutorul
	 * Regular Expressions (sau regeX) se sparg cele doua polinoame preluate sub forma de String-uri
	 * in grupuri ce constau in coeficientul unui monom, respectiv gradul unui monom. Acestea din urma
	 * se vor adauga obiectelor de tip Polinom, creand astfel cele doua obiecte Polinom, cu care se
	 * vor lucra.
	 */
	public void doAdunare()
	{
		String polinom1 = firstPolField.getText();
		String polinom2 = secondPolField.getText();
		String patternString = "(-?\\b\\d+)[xX]\\^(\\+?\\d+\\b)";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(polinom1);
		Matcher matcher2 = pattern.matcher(polinom2);
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		
		while(matcher.find())
		{
			int exponent = Integer.parseInt(matcher.group(2));
			double coeficient = Double.parseDouble(matcher.group(1));
			Monom m = new Monom(exponent,coeficient);
			p1.addMonom(m);
		}
		
		while(matcher2.find())
		{
			int exponent = Integer.parseInt(matcher2.group(2));
			double coeficient = Double.parseDouble(matcher2.group(1));
			Monom m = new Monom(exponent,coeficient);
			p2.addMonom(m);
		}
		rezultatField.setText(Operatie.adunare(p1, p2));
	}
	
	public void doScadere()
	{
		String polinom1 = firstPolField.getText();
		String polinom2 = secondPolField.getText();
		String patternString = "(-?\\b\\d+)[xX]\\^(\\+?\\d+\\b)";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(polinom1);
		Matcher matcher2 = pattern.matcher(polinom2);
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		
		while(matcher.find())
		{
			int exponent = Integer.parseInt(matcher.group(2));
			double coeficient = Double.parseDouble(matcher.group(1));
			Monom m = new Monom(exponent,coeficient);
			p1.addMonom(m);
		}
		
		while(matcher2.find())
		{
			int exponent = Integer.parseInt(matcher2.group(2));
			double coeficient = Double.parseDouble(matcher2.group(1));
			Monom m = new Monom(exponent,coeficient);
			p2.addMonom(m);
		}
		rezultatField.setText(Operatie.scadere(p1, p2));
	}
	
	public void doInmultire()
	{
		String polinom1 = firstPolField.getText();
		String polinom2 = secondPolField.getText();
		String patternString = "(-?\\b\\d+)[xX]\\^(\\+?\\d+\\b)";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(polinom1);
		Matcher matcher2 = pattern.matcher(polinom2);
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		
		while(matcher.find())
		{
			int exponent = Integer.parseInt(matcher.group(2));
			double coeficient = Double.parseDouble(matcher.group(1));
			Monom m = new Monom(exponent,coeficient);
			p1.addMonom(m);
		}
		
		while(matcher2.find())
		{
			int exponent = Integer.parseInt(matcher2.group(2));
			double coeficient = Double.parseDouble(matcher2.group(1));
			Monom m = new Monom(exponent,coeficient);
			p2.addMonom(m);
		}
		
		rezultatField.setText(Operatie.inmultire(p1, p2));
	}
	
	public void doImpartire()
	{
		String polinom1 = firstPolField.getText();
		String polinom2 = secondPolField.getText();
		String patternString = "(-?\\b\\d+)[xX]\\^(\\+?\\d+\\b)";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(polinom1);
		Matcher matcher2 = pattern.matcher(polinom2);
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		
		while(matcher.find())
		{
			int exponent = Integer.parseInt(matcher.group(2));
			double coeficient = Double.parseDouble(matcher.group(1));
			Monom m = new Monom(exponent,coeficient);
			p1.addMonom(m);
		}
		
		while(matcher2.find())
		{
			int exponent = Integer.parseInt(matcher2.group(2));
			double coeficient = Double.parseDouble(matcher2.group(1));
			Monom m = new Monom(exponent,coeficient);
			p2.addMonom(m);
		}
		rezultatField.setText(Operatie.impartire(p1, p2));
	}
	
	public void doDerivare() throws DerivareException
	{
		String polinom1 = firstPolField.getText();
		String polinom2 = secondPolField.getText();
		String patternString = "(-?\\b\\d+)[xX]\\^(\\+?\\d+\\b)";
		Pattern pattern = Pattern.compile(patternString);
		if(!polinom1.equals("") && !polinom2.equals(""))
		{
			throw new DerivareException("Doar un polinom trebuie scris!");
		}
		if(!polinom1.equals(""))
		{
			Matcher matcher = pattern.matcher(polinom1);
			Polinom p1 = new Polinom();
			while(matcher.find())
			{
				int exponent = Integer.parseInt(matcher.group(2));
				double coeficient = Double.parseDouble(matcher.group(1));
				Monom m = new Monom(exponent,coeficient);
				p1.addMonom(m);
			}
			
			rezultatField.setText(Operatie.derivare(p1));
		}
		else if(!polinom2.equals(""))
		{
			Matcher matcher2 = pattern.matcher(polinom2);
			Polinom p2 = new Polinom();
			while(matcher2.find())
			{
				int exponent = Integer.parseInt(matcher2.group(2));
				double coeficient = Double.parseDouble(matcher2.group(1));
				Monom m = new Monom(exponent,coeficient);
				p2.addMonom(m);
			}
			rezultatField.setText(Operatie.derivare(p2));

		}
		else
		{
			throw new DerivareException("Ambele polinoame sunt nule!");
		}
	}
	
	public void doIntegrare() throws IntegrareException
	{
		String polinom1 = firstPolField.getText();
		String polinom2 = secondPolField.getText();
		String patternString = "(-?\\b\\d+)[xX]\\^(\\+?\\d+\\b)";
		Pattern pattern = Pattern.compile(patternString);
		if(!polinom1.equals("") && !polinom2.equals(""))
		{
			throw new IntegrareException("Doar un polinom trebuie scris!");
		}
		if(!polinom1.equals(""))
		{
			Matcher matcher = pattern.matcher(polinom1);
			Polinom p1 = new Polinom();
			while(matcher.find())
			{
				int exponent = Integer.parseInt(matcher.group(2));
				double coeficient = Double.parseDouble(matcher.group(1));
				Monom m = new Monom(exponent,coeficient);
				p1.addMonom(m);
			}
			
			rezultatField.setText(Operatie.integrare(p1));
		}
		else if(!polinom2.equals(""))
		{
			Matcher matcher2 = pattern.matcher(polinom2);
			Polinom p2 = new Polinom();
			while(matcher2.find())
			{
				int exponent = Integer.parseInt(matcher2.group(2));
				double coeficient = Double.parseDouble(matcher2.group(1));
				Monom m = new Monom(exponent,coeficient);
				p2.addMonom(m);
			}
			rezultatField.setText(Operatie.integrare(p2));
		}
		else
		{
			throw new IntegrareException("Ambele polinoame sunt nule!");
		}
	}
}
