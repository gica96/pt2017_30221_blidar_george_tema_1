import java.util.Comparator;
/**
 * Class Name: Monom
 * Descriere: Cu ajutorul acestei clase se vor instantia obiectele de tip Monom. Clasa contine 
 * doua campuri de variabile instanta: exponent si coeficient. Constructorul initializeaza cele doua
 * variabile cu argumentele transmise in momentul instantierii unui nou obiect. Sunt prezente si 
 * getters si setters.
 */
public class Monom {
	
	private int exponent;
	private double coeficient;
	
	public Monom(int exp, double coef)
	{
		this.exponent = exp;
		this.coeficient = coef;
	}
	
	public int getExp()
	{
		return this.exponent;
	}
	
	public double getCoef()
	{
		return this.coeficient;
	}
	
	/**
	 * Class name: compareByGrade
	 * Descriere: Este o clasa statica anonima (Annonymous Inner Class), ce creaza un nou
	 * obiect de tip Comparator, care ne va asigura de fiecare data, ca monoamele din ArraList-ul
	 * polinoamelor vor fi aranjate in ordine descrescatoare dupa gradul lor, indiferent de cum le-a
	 * introdus utilizatorul in cele doua campuri.
	 */
	public static final Comparator<Monom> compareByGrade = new Comparator<Monom>()
			{
				public int compare(Monom m1, Monom m2)
				{
					int number1 = m1.getExp();
					int number2 = m2.getExp();
					return number2-number1;
				}
			};
	
	/**
	 * Methods name: inmulrieMonom, scadereMonom, impareMonom
	 * Descriere: Sunt trei metode statice folosite pentru a executa anumite operatii intre doua
	 * monoame.
	 */
	public static Monom inmultireMonom(Monom m1, Monom m2)
	{
		Monom temporar = new Monom(m1.getExp() + m2.getExp(),m1.getCoef() * m2.getCoef());
		return temporar;
	}
	
	public static Monom scadereMonom(Monom m1, Monom m2)
	{
		Monom temporar = new Monom(m1.getExp(),m1.getCoef() - m2.getCoef());
		return temporar;
	}
	
	public static Monom imparteMonom(Monom m1, Monom m2)
	{
		Monom temporar = new Monom(m1.getExp() - m2.getExp(),m1.getCoef() / m2.getCoef());
		return temporar;
	}
}
