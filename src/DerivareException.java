/**
 * Class Name: DerivareException
 * Descriere: O simpla clasa de exceptie, care are rolul de a lasa doar introducerea unui
 * singur polinom in aplicatie, deoarece operatia de derivare lucreaza cu un singur polinom
 */
public class DerivareException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public DerivareException(String message)
	{
		super(message);
	}
}
