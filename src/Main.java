/**
 * Class Name: Main
 * Descriere: Este clasa care contine metoda statica main, care va executa programul prin apelul
 * metodei statice din clasa CalculatorGUI - triggerWindow().
 *
 */
public class Main {
	
	public static void main(String[] args)
	{
		CalculatorGUI.triggerWindow();
	}

}
