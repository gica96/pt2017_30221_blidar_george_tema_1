import java.util.Collections;

/** 
 * Class Name: Operatie
 * Descriere: Este o clasa ce contine metodele statice necesare implementarii operatiilor intre doua
 * polinoame
 */
public class Operatie {
		
	/**
	 * Method Name: adunare
	 * @param p1 = primul polinom
	 * @param p2 = al doilea polinom
	 * @return = al treilea polinom returnat ca String
	 * Descriere: Simpla adunare a doua polinoame: cat timp n-am ajuns cu niciun index la capatul
	 * vreunui polinom, continuam sa le adunam comparand gradele celor doua monoame curente.
	 * La final, daca un polinom este mai mic ca si lungime decat celalalt, se parcurg restul 
	 * monoamelor ramase si se adauga la polinomul rezultat.
	 */
	public static String adunare(Polinom p1, Polinom p2)
	{
		Polinom rezultat = new Polinom();
		int i = 0;
		int j = 0;
		
		Collections.sort(p1.getPolinom(),Monom.compareByGrade);
		Collections.sort(p2.getPolinom(),Monom.compareByGrade);
		
		while( j < p2.getPolinom().size() && i < p1.getPolinom().size())
		{
			
			if(p1.getPolinom().get(i).getExp() > p2.getPolinom().get(j).getExp())
			{
				Monom m = new Monom(p1.getPolinom().get(i).getExp(),p1.getPolinom().get(i).getCoef());
				rezultat.addMonom(m);
				i++;
			}
			else if(p1.getPolinom().get(i).getExp() < p2.getPolinom().get(j).getExp())
			{
				Monom m = new Monom(p2.getPolinom().get(j).getExp(),p2.getPolinom().get(j).getCoef());
				rezultat.addMonom(m);
				j++;
			}
			else
			{
				Monom m = new Monom(p1.getPolinom().get(i).getExp(),p2.getPolinom().get(j).getCoef() + p1.getPolinom().get(i).getCoef());
				rezultat.addMonom(m);
				j++;
				i++;
			}
		}
		
		while( i < p1.getPolinom().size())
		{
			Monom m = new Monom(p1.getPolinom().get(i).getExp(),p1.getPolinom().get(i).getCoef());
			rezultat.addMonom(m);
			i++;
		}
		
		while( j < p2.getPolinom().size())
		{
			Monom m = new Monom(p2.getPolinom().get(j).getExp(),p2.getPolinom().get(j).getCoef());
			rezultat.addMonom(m);
			j++;
		}
		
		String s = rezultat.getPolinomAsString();
		if(s.equals(""))
		{
			return "0";
		}
		else
		{
			return s;
		}
	}
	
	/**
	 * Method name: scadere
	 * @param p1 = primul polinom
	 * @param p2 = al doilea polinom
	 * @return = al treilea polinom returnat ca String
	 * Descreire: Principiul de functionare este asemanator adunarii.
	 */
	public static String scadere(Polinom p1, Polinom p2)
	{
		Polinom rezultat = new Polinom();
		int i = 0;
		int j = 0;
		
		Collections.sort(p1.getPolinom(),Monom.compareByGrade);
		Collections.sort(p2.getPolinom(),Monom.compareByGrade);
		
		while( j < p2.getPolinom().size() && i < p1.getPolinom().size())
		{
			if(p1.getPolinom().get(i).getExp() > p2.getPolinom().get(j).getExp())
			{
				Monom m = new Monom(p1.getPolinom().get(i).getExp(),p1.getPolinom().get(i).getCoef());
				rezultat.addMonom(m);
				i++;
			}
			else if(p1.getPolinom().get(i).getExp() < p2.getPolinom().get(j).getExp())
			{
				Monom m = new Monom(p2.getPolinom().get(j).getExp(),0-p2.getPolinom().get(j).getCoef());
				rezultat.addMonom(m);
				j++;
			}
			else
			{
				Monom m = new Monom(p1.getPolinom().get(i).getExp(),p1.getPolinom().get(i).getCoef() - p2.getPolinom().get(j).getCoef());
				rezultat.addMonom(m);
				j++;
				i++;
			}
		}
		
		while( i < p1.getPolinom().size())
		{
			Monom m = new Monom(p1.getPolinom().get(i).getExp(),p1.getPolinom().get(i).getCoef());
			rezultat.addMonom(m);
			i++;
		}
		
		while( j < p2.getPolinom().size())
		{
			Monom m = new Monom(p2.getPolinom().get(j).getExp(),-p2.getPolinom().get(j).getCoef());
			rezultat.addMonom(m);
			j++;
		}
		
		Collections.sort(rezultat.getPolinom(),Monom.compareByGrade);
		String s = rezultat.getPolinomAsString();
		if(s.equals(""))
		{
			return "0";
		}
		else
		{
			return s;
		}
	}
	
	/**
	 * Method name: scadereP
	 * @param p1 = primul polinom
	 * @param p2 = al doilea polinom
	 * @return rezultat = al treilea polinom returnat ca si Polinom
	 * Descriere: Este exact aceeasi metoda cu cea de mai sus, doar ca retunreaza un obiect
	 * de tip Polinom, si nu String. Este folosita la impartirea a doua polinoame, in momentul
	 * scaderii "D-aux".
	 */
	public static Polinom scadereP(Polinom p1, Polinom p2)
	{
		Polinom rezultat = new Polinom();
		int i = 0;
		int j = 0;
		
		Collections.sort(p1.getPolinom(),Monom.compareByGrade);
		Collections.sort(p2.getPolinom(),Monom.compareByGrade);
		
		while( j < p2.getPolinom().size() && i < p1.getPolinom().size())
		{
			if(p1.getPolinom().get(i).getExp() > p2.getPolinom().get(j).getExp())
			{
				Monom m = new Monom(p1.getPolinom().get(i).getExp(),p1.getPolinom().get(i).getCoef());
				rezultat.addMonom(m);
				i++;
			}
			else if(p1.getPolinom().get(i).getExp() < p2.getPolinom().get(j).getExp())
			{
				Monom m = new Monom(p2.getPolinom().get(j).getExp(),-p2.getPolinom().get(j).getCoef());
				rezultat.addMonom(m);
				j++;
			}
			else
			{
				Monom m = new Monom(p1.getPolinom().get(i).getExp(),p1.getPolinom().get(i).getCoef() - p2.getPolinom().get(j).getCoef());
				if(m.getCoef() != 0)
				{
					rezultat.addMonom(m);
				}
				j++;
				i++;
			}
		}
		
		
		while( i < p1.getPolinom().size())
		{
			Monom m = new Monom(p1.getPolinom().get(i).getExp(),p1.getPolinom().get(i).getCoef());
			rezultat.addMonom(m);
			i++;
		}
		
		while( j < p2.getPolinom().size())
		{
			Monom m = new Monom(p2.getPolinom().get(j).getExp(),-p2.getPolinom().get(j).getCoef());
			rezultat.addMonom(m);
			j++;
		}
		
		return rezultat;
	}
	
	/**
	 * Method name: inmultire
	 * @param p1 = primul polinom 
	 * @param p2 = al doilea polinom
	 * @return = al treilea polinom returnat ca String
	 * Descriere: Se inmulteste fiecare monom din primul polinom cu fiecare monom din al doilea
	 * iar in final, monoamele cu grad identic, daca exista, se aduna intre ele si formeaza 
	 * polinomul rezultat.
	 */
	
	public static String inmultire(Polinom p1, Polinom p2)
	{
		Polinom rezultat = new Polinom();
		int i = 0;
		int j = 0;
		Collections.sort(p1.getPolinom(),Monom.compareByGrade);
		Collections.sort(p2.getPolinom(),Monom.compareByGrade);
		
		while( i < p1.getPolinom().size())
		{
			j = 0;
			while(j < p2.getPolinom().size())
			{
				Monom m = new Monom(p1.getPolinom().get(i).getExp() + p2.getPolinom().get(j).getExp(),p1.getPolinom().get(i).getCoef() * p2.getPolinom().get(j).getCoef());
				rezultat.addMonom(m);
				j++;
			}
			i++;
		}
		
		i = 0;
		j = 0;
		Polinom pFinal = new Polinom();
		
		while(i < rezultat.getPolinom().size())
		{
			double tempCoef = rezultat.getPolinom().get(i).getCoef();
			j = i + 1;
			while( j < rezultat.getPolinom().size())
			{
				if(rezultat.getPolinom().get(j).getExp() == rezultat.getPolinom().get(i).getExp())
				{
					tempCoef = tempCoef + rezultat.getPolinom().get(j).getCoef();
					j++;
				}
				else
				{
					j++;
				}
			}
			Monom m = new Monom(rezultat.getPolinom().get(i).getExp(),tempCoef);
			pFinal.addMonom(m);
			i++;
			while(i < rezultat.getPolinom().size() && rezultat.getPolinom().get(i).getExp() == rezultat.getPolinom().get(i-1).getExp())
			{
				i++;
			}
		}
		Collections.sort(pFinal.getPolinom(),Monom.compareByGrade);
		String s = pFinal.getPolinomAsString();
		if(s.equals(""))
		{
			return "0";
		}
		else
		{
			return s;
		}
	}
	
	/* O alta posibilitate de a realiza metoda de inmultire a doua polinoame.
	 public static String inmultire(Polinom p1, Polinom p2)
	{
		Polinom rezultat = new Polinom();
		int i = 0;
		int j = 0;
		Collections.sort(p1.getPolinom(),Monom.compareByGrade);
		Collections.sort(p2.getPolinom(),Monom.compareByGrade);
		
		while( i < p1.getPolinom().size())
		{
			j = 0;
			while(j < p2.getPolinom().size())
			{
				Monom m = new Monom(p1.getPolinom().get(i).getExp() + p2.getPolinom().get(j).getExp(),p1.getPolinom().get(i).getCoef() * p2.getPolinom().get(j).getCoef());
				rezultat.addMonom(m);
				j++;
			}
			i++;
		}
		
		i = 0;
		Polinom pFinal = new Polinom();
		
		while(i < rezultat.getPolinom().size())
		{
			double tempCoef = rezultat.getPolinom().get(i).getCoef();
			j = i + 1;
			while( j < rezultat.getPolinom().size())
			{
				if(rezultat.getPolinom().get(j).getExp() == rezultat.getPolinom().get(i).getExp())
				{
					tempCoef = tempCoef + rezultat.getPolinom().get(j).getCoef();
					rezultat.getPolinom().remove(j);
					j++;
				}
				else
				{
					j++;
				}
			}
			Monom m = new Monom(rezultat.getPolinom().get(i).getExp(),tempCoef);
			pFinal.addMonom(m);
			i++;
		}
		Collections.sort(pFinal.getPolinom(),Monom.compareByGrade);
		String s = pFinal.getPolinomAsString();
		if(s.equals(""))
		{
			return "0";
		}
		else
		{
			return s;
		}
	}
	 */
	
	/**
	 * Method name: impartire
	 * @param p1 = primul polinom
	 * @param p2 = al doilea polinom
	 * @return = al treilea polinom returnat ca String
	 * Descriere: Cat timp gradul deimpartitului este mai mare sau egal cu gradul impartitorului,
	 * se formeaza un monom "cn" intermediar prin impartirea monomului de grad maxim al deimpartitului
	 * cu monomul de grad maxim al impartitoului. Acesta se inmulteste, pe rand, cu toate monomale
	 * impartitorului, rezultand polinomul auxiliar. Se adauga monomul "cn" la polinomul "cat" si pe 
	 * urma se scade din deimpartit, polinomul auxiliar, rezultand un nou deimpartit.
	 */
	public static String impartire(Polinom p1, Polinom p2)
	{
		
		if(p2.getPolinomAsString().equals(""))
		{
			throw new IllegalArgumentException("Impartitorul nu poate fi 0!");
		}
		Polinom aux = new Polinom();
		Polinom cat = new Polinom();
		Polinom deimpartit = p1;
		int i = 0;
		boolean ok = true;
		String s;
		
		Collections.sort(p1.getPolinom(),Monom.compareByGrade);
		Collections.sort(p2.getPolinom(),Monom.compareByGrade);
		
		if(deimpartit.getPolinom().get(0).getExp() < p2.getPolinom().get(0).getExp())
		{
			s = "Catul este 0 iar restul este " + p1.getPolinomAsString();
			return s;
		}
		
		while(deimpartit.getPolinom().get(0).getExp() >= p2.getPolinom().get(0).getExp())
		{
			Monom cn = Monom.imparteMonom(deimpartit.getPolinom().get(0), p2.getPolinom().get(0));
			while(i < p2.getPolinom().size())
			{
				Monom inmultit = Monom.inmultireMonom(cn, p2.getPolinom().get(i));
				aux.addMonom(inmultit);
				i++;
			}
			i = 0;
			cat.addMonom(cn);
			deimpartit = Operatie.scadereP(deimpartit, aux);
			if(deimpartit.getPolinom().isEmpty())
			{
				ok = false;
				break;
			}
			aux = new Polinom();
		}
		if( ok == false)
		{
			s = "Catul este: " + cat.getPolinomAsString() + " iar restul este 0" ;
		}
		else
		{
			 s = "Catul este: " + cat.getPolinomAsString() + " iar restul este: " + deimpartit.getPolinomAsString();
		}
		return s;
	}
	
	/**
	 * Methods name: derivare, integrare
	 * Sunt doua metode care se ocupa de realizarea operatiilor de integrare si derivare a unui
	 * polinom primit ca si parametru.
	 */
	public static String derivare(Polinom p)
	{
		Collections.sort(p.getPolinom(),Monom.compareByGrade);
		int i = 0;
		Polinom rezultat = new Polinom();
		while(i < p.getPolinom().size())
		{
			if(p.getPolinom().get(i).getExp() != 0)
			{
				Monom m = new Monom(p.getPolinom().get(i).getExp()-1,p.getPolinom().get(i).getCoef() * p.getPolinom().get(i).getExp());
				rezultat.addMonom(m);
				i++;
			}
			else
			{
				i++;
			}
		}
		
		String s = rezultat.getPolinomAsString();
		if(s.equals(""))
		{
			return "0";
		}
		else
		{
			return s;
		}
	}
	
	public static String integrare(Polinom p)
	{
		Collections.sort(p.getPolinom(),Monom.compareByGrade);
		int i = 0;
		Polinom rezultat = new Polinom();
		while(i < p.getPolinom().size())
		{
			if(p.getPolinom().get(i).getExp() == -1)
			{
				throw new IllegalArgumentException("Logaritm natural din X!");
			}
			else
			{
				Monom m = new Monom(p.getPolinom().get(i).getExp()+1,p.getPolinom().get(i).getCoef() / (p.getPolinom().get(i).getExp()+1));
				rezultat.addMonom(m);
				i++;
			}
		}
		String s = rezultat.getPolinomAsString();
		if(s.equals(""))
		{
			return "0";
		}
		else
		{
			return s + "+C";
		}
	}
}
