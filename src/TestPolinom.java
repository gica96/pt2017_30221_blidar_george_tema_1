
import org.junit.Test;

import junit.framework.TestCase;

/**
 * Class name: TestPolinom
 * Descriere: Este o clasa folosita pentru a realiza testarile unitare pentru cele sase operatii ce se 
 * pot exercita asupra polinoamelor.
 */
public class TestPolinom extends TestCase {
	
	@Test
	public void test() 
	{
		Polinom p = new Polinom();
		
		Monom m=new Monom(4,2);
		p.addMonom(m);
		
		Polinom p2 = new Polinom();
		
		m = new Monom(2,2);
		p2.addMonom(m);
		
		m = new Monom(4,3);
		p2.addMonom(m);
		
		String expected = "5.0x^4+2.0x^2";
		
		assertEquals(expected,Operatie.adunare(p,p2));
	}
	
	@Test
	public void test1() 
	{
		Polinom p = new Polinom();
		
		Monom m = new Monom(3,1);
		p.addMonom(m);
		
		m = new Monom(2,-2);
		p.addMonom(m);
		
		m = new Monom(0,5);
		p.addMonom(m);
		
		Polinom p2 = new Polinom();
		
		m = new Monom(2,3);
		p2.addMonom(m);
		
		m = new Monom(1,3);
		p2.addMonom(m);
		
		String expected = "1.0x^3-5.0x^2-3.0x^1+5.0x^0";
		
		assertEquals(expected,Operatie.scadere(p,p2));
	}
	
	@Test
	public void test2() 
	{
		Polinom p = new Polinom();
		
		Monom m = new Monom(3,-1);
		p.addMonom(m);
		
		m = new Monom(2,1);
		p.addMonom(m);
		
		m = new Monom(1,4);
		p.addMonom(m);
		
		Polinom p2 = new Polinom();
		
		m = new Monom(5,3);
		p2.addMonom(m);
		
		m = new Monom(4,1);
		p2.addMonom(m);
		
		String expected = "-3.0x^8+2.0x^7+13.0x^6+4.0x^5";
		assertEquals(expected,Operatie.inmultire(p,p2));
	}
	
	@Test
	public void test3() 
	{
		Polinom p = new Polinom();
		
		Monom m = new Monom(4,1);
		p.addMonom(m);
		
		m = new Monom(0,2);
		p.addMonom(m);
		
		Polinom p2 = new Polinom();
		
		m = new Monom(3,1);
		p2.addMonom(m);
		
		m = new Monom(2,1);
		p2.addMonom(m);
		
		m = new Monom(1,1);
		p2.addMonom(m);
		
		m = new Monom(0,1);
		p2.addMonom(m);
		
		String expected = "Catul este: 1.0x^1-1.0x^0 iar restul este: 3.0x^0";
		assertEquals(expected,Operatie.impartire(p,p2));
	}
	
	@Test
	public void test4() 
	{
		Polinom p = new Polinom();
		
		Monom m = new Monom(3,-5);
		p.addMonom(m);
		
		m = new Monom(2,1);
		p.addMonom(m);
		
		m = new Monom(1,4);
		p.addMonom(m);
				
		String expected = "-15.0x^2+2.0x^1+4.0x^0";
		assertEquals(expected,Operatie.derivare(p));
	}
	
	@Test
	public void test5() 
	{
		Polinom p = new Polinom();
		
		Monom m = new Monom(4,2);
		p.addMonom(m);
		
		m = new Monom(3,7);
		p.addMonom(m);
		
		m = new Monom(0,2);
		p.addMonom(m);
				
		String expected = "0.4x^5+1.75x^4+2.0x^1+C";
		assertEquals(expected,Operatie.integrare(p));
	}
	
}
