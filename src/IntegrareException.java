/**
 * Class Name: IntegrareException
 * Descriere: O simpla clasa de exceptie, care are rolul de a lasa doar introducerea unui
 * singur polinom in aplicatie, deoarece operatia de integrare lucreaza cu un singur polinom.
 */
public class IntegrareException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public IntegrareException(String message)
	{
		super(message);
	}
}
