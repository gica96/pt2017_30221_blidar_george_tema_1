import java.util.ArrayList;

/**
 * Class name: Polinom
 * Descriere: Reprezinta modelarea unui polinom, avand ca si variabila instanta un ArrayList de
 * monoame. Contine metoda de adaugare a unui monom in acest ArrayList (addMonom), un getter
 * ce returneaza acest ArrayList si o metoda asemanatoare unui toString care returneaza polinomul
 * sub forma matematica, ca un String.
 */
public class Polinom {
	
	private ArrayList<Monom> monom;
	
	public Polinom()
	{
		monom = new ArrayList<Monom>();
	}
	
	public void addMonom(Monom m)
	{
		this.monom.add(m);
	}
	
	public ArrayList<Monom> getPolinom()
	{
		return this.monom;
	}
	
	public String getPolinomAsString()
	{
		String string ="";
		int p = 0;
		for(Monom index : monom)
		{
			if(p != 0)
			{
				if(index.getCoef() > 0)
				{
					string = string + "+";
					string = string + index.getCoef() + "x^" + index.getExp();
				}
				else if(index.getCoef() < 0)
				{
					string = string + index.getCoef() + "x^" + index.getExp();
				}
				else
				{
					string = string +"";
				}
			}
			else if(index.getCoef() == 0)
			{
				string = string + "";
				p=1;
			}
			else
			{
				string = string + index.getCoef() + "x^" + index.getExp();
				p=1;
			}
			
		}
		return string;
	}

}
